O:Today, our main content is group presentation and Code refactoring. In the group presentation, we demonstrated the contents of design patterns, which deepened our impression of Observer pattern, Strategy pattern and Command pattern. Then we began to learn about Code refactoring. The teacher introduced several scenarios that require Code refactoring, and let us practice this.

R:I feel a bit hard.

I:Code refactoring is a strange field for me. In the process of writing code before, I seldom take the initiative to perform Code refactoring, and often just write code to complete business. Today's course let me understand the importance of Code refactoring, but to be familiar with this operation, I still need to keep learning.

D:After understanding the importance of Code refactoring, I also realized my lack of knowledge in this area. I will continue to learn this aspect and fill in the gaps in knowledge.