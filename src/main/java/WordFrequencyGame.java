import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String SPLIT_EMPTY = "\\s+";
    public static final int INITIAL_VALUE = 1;
    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> unArrangedWordFrequencyList = getUnArrangedInputList(inputStr);
            Map<String, Integer> inputListMap = convertInputListToMap(unArrangedWordFrequencyList);
            List<WordFrequency> arrangedList = getArrangedInputList(inputListMap);
            getArrangedAndSortedInputList(arrangedList);
            return getInputListString(arrangedList);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private static String getInputListString(List<WordFrequency> list) {
        StringJoiner joiner = new StringJoiner("\n");
        for (WordFrequency word : list) {
            joiner.add(word.getValue() + " " + word.getWordCount());
        }
        return joiner.toString();
    }

    private static void getArrangedAndSortedInputList(List<WordFrequency> list) {
        list.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }

    private static List<WordFrequency> getArrangedInputList(Map<String, Integer> map) {
        List<WordFrequency> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            list.add(new WordFrequency(entry.getKey(), entry.getValue()));
        }
        return list;
    }

    private static List<WordFrequency> getUnArrangedInputList(String inputStr) {
        return Arrays
                .stream(inputStr.split(SPLIT_EMPTY))
                .map(word -> new WordFrequency(word, INITIAL_VALUE))
                .collect(Collectors.toList());
    }


    private Map<String, Integer> convertInputListToMap(List<WordFrequency> wordFrequencyList) {
        Map<String, Integer> map = new HashMap<>();
        for (WordFrequency wordFrequency : wordFrequencyList) {
            if (!map.containsKey(wordFrequency.getValue())) {
                map.put(wordFrequency.getValue(), INITIAL_VALUE);
            } else {
                map.put(wordFrequency.getValue(), map.get(wordFrequency.getValue()) + 1);
            }
        }
        return map;
    }


}
